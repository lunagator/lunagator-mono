// EarthMoon - 
//  Explore spacecraft trajectories in the vicinity of the Earth and Moon.

using System;
using System.Linq;
using Gtk;

partial class EarthMoon : Window
{
    public EMCRTBP tbp = new EMCRTBP();
    private readonly PanelConstructors ppcInitial = new InitialPositionPanels();
    private readonly PanelConstructors ppcTraj = new TrajectoryPanels();
    private readonly PanelConstructors ppcFinal = new FinalPositionPanels();
    private int ptInitial = 0;
    private int ptTraj = 0;
    private int ptFinal = 0;
    private ParamPanel ppInitial;
    private ParamPanel ppFinal;
    private TrajectoryPanel ppTraj;
    DrawingArea aPlot = new DrawingArea();
    Label lSummary = new Label();
    private VBox vbParams;
    private HBox hboxMain;

    public EarthMoon() : base("")
    {
        var vboxMain = new VBox(homogeneous: false, spacing: 2);

        using (var mb = new MenuBar())
        {
            var agr = new AccelGroup();
            FillMenuBar(mb, agr);
            AddAccelGroup(agr);
            vboxMain.PackStart(mb, false, false, 0);
        }
        hboxMain = new HBox(false, 2);
        vbParams = new VBox(false, 2);
        FillParamBox();
        hboxMain.PackStart(vbParams, false, false, 0);

        using (var vbPlot = new VBox(false, 2))
        {
            FillPlotPanel(vbPlot);
            hboxMain.PackStart(vbPlot, true, true, 0);
        }
        vboxMain.PackStart(hboxMain, true, true, 0);

        Add(vboxMain);

        SetDefaultSize(width: 800, height: 400);
        SetPosition(WindowPosition.Center);
        ShowAll();

        DeleteEvent += OnExit;
    }

    private void FillPlotPanel(VBox vb)
    {
        vb.PackStart(aPlot, expand: true, fill: true, padding: 0);
        aPlot.ExposeEvent += OnExpose;
        lSummary.SetAlignment(1.0F, 0.5F);
        vb.PackStart(lSummary, false, false, 0);
    }

    class SubPanelButton
    {
        public String text;
        public EventHandler e;
        public SubPanelButton(String text, EventHandler e) 
        {
            this.text = text; this.e = e;
        }
    };
    private ParamPanel AddComboSubPanel(VBox vb, String strTitle,
        PanelConstructors ppc, int pt, EventHandler fChanged,
        Boolean IsEditable, SubPanelButton spb = null, Boolean ShowTime = false)
    {
        ParamPanel pp;
        vb.PackStart(new Label(strTitle), false, false, 0);
        var cbAlternatives = new ComboBox(ppc.NamesOfTypes());
        cbAlternatives.Active = pt;
        vb.PackStart(cbAlternatives, false, false, 0);
        cbAlternatives.Changed += fChanged;
        VBox[] cols = { new VBox(), new VBox() };
        using (var hboxCols = new HBox(false, 0))
        {
            pp = ppc.NewPanelOfType(pt);
            vb.PackStart(hboxCols, false, false, 0);
            foreach (VBox v in cols) hboxCols.PackStart(v, false, false, 0);
            foreach (Label l in pp.ls) cols[0].PackStart(l, true, false, 0);
            foreach (int i in Enumerable.Range(0, pp.es.Length))
            {
                cols[1].PackStart(pp.es[i], true, true, 0);
                pp.es[i].IsEditable = IsEditable;
            }
        }
        if (ShowTime)
        {
            Label l = new Label("t:");
            pp.eTime = new Entry();
            pp.eTime.IsEditable = false;
            cols[0].PackStart(l, true, false, 0);
            cols[1].PackStart(pp.eTime, true, true, 0);
        }
        if (spb != null)
        {
            pp.panelButton = new Button(spb.text);
            vbParams.PackStart(pp.panelButton, false, false, 0);
            pp.panelButton.Clicked += spb.e;
        }
        return pp;
    }

    private void OnICBChanged(object sender, EventArgs e)
    {
        ComboBox cb = (ComboBox)sender;
        ptInitial = cb.Active;
        RefillParamBox();
        ShowAll();
    }

    private void OnTCBChanged(object sender, EventArgs e)
    {
        ComboBox cb = (ComboBox)sender;
        ptTraj = cb.Active;
        RefillParamBox();
        ShowAll();
        aPlot.QueueDraw();
    }

    private void OnFCBChanged(object sender, EventArgs e)
    {
        ComboBox cb = (ComboBox)sender;
        ptFinal = cb.Active;
        RefillParamBox();
        ShowAll();
    }

    private void RefillParamBox()
    {
        foreach (Widget w in vbParams.AllChildren)
            vbParams.Remove(w);
        FillParamBox();
    }

    private void FillParamBox()
    {
        ppInitial = AddComboSubPanel(vbParams, "Initial State", ppcInitial,
            ptInitial, OnICBChanged, true, new SubPanelButton("Set", OnSet));
        vbParams.PackStart(new HSeparator(), true, true, 0);
        ppTraj = (TrajectoryPanel)AddComboSubPanel(vbParams, "End At", ppcTraj,
            ptTraj, OnTCBChanged, true, new SubPanelButton("Set", OnSet));
        vbParams.PackStart(new HSeparator(), true, true, 0);
        ppFinal = AddComboSubPanel(vbParams, "Final State", ppcFinal, ptFinal, 
            OnFCBChanged, false, new SubPanelButton("Transfer", OnTransfer), 
            true);
        vbParams.PackStart(new HSeparator(), true, true, 0);
        var bReset = new Button("Reset");
        vbParams.PackStart(bReset, false, false, 0);
        bReset.Clicked += OnReset;

        UpdateParamPanel();
    }

    private void OnRestore(object sender, EventArgs e) =>
        throw new NotImplementedException();

    private void OnStore(object sender, EventArgs e) =>
        throw new NotImplementedException();

    private void OnReset(object sender, EventArgs e)
    {
        UpdateParamPanel();
    }

    public void OnSet(object sender, EventArgs e)
    {
        ppInitial.SetIntoTBP(tbp);
        ppTraj.SetIntoTBP(tbp);
        aPlot.QueueDraw();
    }

    private void OnTransfer(object sender, EventArgs e)
    {
        ppInitial.ResetFormats();
        ppFinal.SetIntoTBP(tbp);
        aPlot.QueueDraw();
    }

    private void FillMenuBar(MenuBar mb, AccelGroup agr)
    {
        var exit = new MenuItem("E_xit");
        exit.AddAccelerator("activate", agr,
            new AccelKey(Gdk.Key.Q, Gdk.ModifierType.ControlMask,
            AccelFlags.Visible));
        exit.Activated += OnExit;

        using (var file = new MenuItem("_File"))
        {
            var fileItems = new Menu();
            fileItems.Append(exit);
            file.Submenu = fileItems;
            mb.Append(file);
        }

        var view = new MenuItem("_View");
        view.AddAccelerator("activate", agr,
            new AccelKey(Gdk.Key.S, Gdk.ModifierType.ControlMask,
            AccelFlags.Visible));
        view.Activated += OnView;

        var integration = new MenuItem("_Integration");
        integration.AddAccelerator("activate", agr,
            new AccelKey(Gdk.Key.I, Gdk.ModifierType.ControlMask,
            AccelFlags.Visible));
        integration.Activated += OnIntegration;

        using (var settings = new MenuItem("_Settings"))
        {
            var settingsItems = new Menu();
            settingsItems.Append(view);
            settingsItems.Append(integration);
            settings.Submenu = settingsItems;
            mb.Append(settings);
        }

        var about = new MenuItem("_About");
        about.AddAccelerator("activate", agr,
            new AccelKey(Gdk.Key.A, Gdk.ModifierType.ControlMask,
            AccelFlags.Visible));
        about.Activated += OnAbout;

        using (var help = new MenuItem("_Help"))
        {
            var helpItems = new Menu();
            helpItems.Append(about);
            help.Submenu = helpItems;
            mb.Append(help);
        }
    }

    private void UpdateParamPanel()
    {
        ppInitial.GetFromTBP(tbp);
        ppFinal.GetFromTBP(tbp);
        ppTraj.GetFromTBP(tbp);
    }

    private void OnExpose(object sender, ExposeEventArgs args)
    {
        var a = (DrawingArea)sender;
        Cairo.Context g = Gdk.CairoHelper.Create(a.GdkWindow);
        tbp.Draw(g, a.Allocation.Width, a.Allocation.Height, ppTraj.End);
        UpdateParamPanel();
        UpdateSummaryLines();
        g.GetTarget().Dispose();
        g.Dispose();
    }

    private void UpdateSummaryLines()
    {
        String strSummary = "Apogee: {0:##0.0#######}, " +
            "Perigee: {1:##0.0#######}\nApolune: {2:##0.0#######}, " +
            "Perilune: {3:##0.0#######}";
        lSummary.Text = string.Format(strSummary,
            tbp.ApsisSummary.Apogee,
            tbp.ApsisSummary.Perigee,
            tbp.ApsisSummary.Apolune,
            tbp.ApsisSummary.Perilune);
    }

    private static void OnExit(object sender, EventArgs e)
    {
        Application.Quit();
    }
}
