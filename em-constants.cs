﻿public class EMConst
{
    private static readonly double EarthMass = 5.9724;
    private static readonly double MoonMass = 0.07346;
    private static readonly double Mu = MoonMass / (EarthMass + MoonMass);

    public static readonly double EmDistance = 385000.0;
    public readonly double EarthRadius = 6378.1 / EmDistance;
    public readonly double MoonRadius = 1738.1 / EmDistance;
    public readonly double MuMoon = Mu;
    public readonly double MuEarth = 1 - Mu;
}

public class ClrConst
{
    public readonly Cairo.Color EarthColor = new Cairo.Color(0.3, 0.4, 0.6);
    public readonly Cairo.Color MoonColor = new Cairo.Color(0.3, 0.3, 0.3);
   
    static readonly Cairo.Color clrRed = new Cairo.Color(0.75, 0.0, 0.0);
    static readonly Cairo.Color clrOrange = new Cairo.Color(0.75, 0.5, 0.0);
    static readonly Cairo.Color clrYellow = new Cairo.Color(0.75, 0.75, 0.0);
    static readonly Cairo.Color clrYellowGreen = new Cairo.Color(0.5, 0.75, 0.0);
    static readonly Cairo.Color clrGreen = new Cairo.Color(0.0, 0.75, 0.0);
    static readonly Cairo.Color clrGreenCyan = new Cairo.Color(0.0, 0.75, 0.5);
    static readonly Cairo.Color clrCyan = new Cairo.Color(0.0, 0.75, 0.75);
    static readonly Cairo.Color clrCyanBlue = new Cairo.Color(0.0, 0.5, 0.75);
    static readonly Cairo.Color clrBlue = new Cairo.Color(0.0, 0.0, 0.75);
    static readonly Cairo.Color clrBlueMangenta = new Cairo.Color(0.5, 0.0, 0.75);
    static readonly Cairo.Color clrMagenta = new Cairo.Color(0.75, 0.0, 0.75);
    static readonly Cairo.Color clrMagentaRed = new Cairo.Color(0.75, 0.0, 0.5);

    public readonly Cairo.Color[] clrsRainbow = {
        clrRed, clrOrange, clrYellow, clrYellowGreen, clrGreen, clrGreenCyan,
        clrCyan, clrCyanBlue, clrBlue, clrBlueMangenta, clrMagenta, clrMagentaRed
    };
}