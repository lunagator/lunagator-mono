﻿using System;

public class Apsides
{
    public double Apogee { get; set; }
    public double Perigee { get; set; }
    public double Apolune { get; set; }
    public double Perilune { get; set; }

    public Apsides()
    {
        Reset();
    }

    public void Update(double rEarth, double rMoon)
    {
        Apogee = Math.Max(Apogee, rEarth);
        Perigee = Math.Min(Perigee, rEarth);
        Apolune = Math.Max(Apolune, rMoon);
        Perilune = Math.Min(Perilune, rMoon);
    }

    public void Reset()
    {
        Apogee = double.MinValue;
        Perigee = double.MaxValue;
        Apolune = double.MinValue;
        Perilune = double.MaxValue;
    }
}
