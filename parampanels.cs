﻿using System;
using System.Linq;
using Gtk;

public class PanelConstructors
{
    public virtual ParamPanel NewPanelOfType(int i) =>
        throw new System.NotImplementedException();
    public virtual String[] NamesOfTypes() =>
        throw new System.NotImplementedException();
}

public class InitialPositionPanels : PanelConstructors
{
    public override ParamPanel NewPanelOfType(int i)
    {
        switch (i)
        {
            case 4: return new InitialLloPanel();
            case 3: return new InitialLeoPanel();
            case 2: return new InitialSelenocentricPanel();
            case 1: return new InitialGeocentricPanel();
            default: return new InitialCartesianPanel();
        }
    }

    public override String[] NamesOfTypes() =>
        new string[] { "Barycentric", "Geocentric", "Selenocentric",
                "Geocentric (km/s)", "Selenocentric (km/s)"};
}

public class FinalPositionPanels : PanelConstructors
{
    public override ParamPanel NewPanelOfType(int i)
    {
        switch (i)
        {
            case 4: return new FinalLloPanel();
            case 3: return new FinalLeoPanel();
            case 2: return new FinalSelenocentricPanel();
            case 1: return new FinalGeocentricPanel();
            default: return new FinalCartesianPanel();
        }
    }

    public override String[] NamesOfTypes() =>
        new string[] { "Barycentric", "Geocentric", "Selenocentric",
                "Geocentric (km/s)", "Selenocentric (km/s)"};
}

public class TrajectoryPanels : PanelConstructors
{
    public override ParamPanel NewPanelOfType(int i)
    {
        switch (i)
        {
            case 5: return new BodyContactDays();
            case 4: return new ApogeeDays();
            case 3: return new PeriluneDays();
            case 2: return new HoursTrajectory();
            case 1: return new DaysTrajectory();
            default: return new SiderealTrajectory();
        }
    }

    public override String[] NamesOfTypes() =>
        new string[] { "Time (lunar months)",
                "Time (days)", "Time (hours)",
                "Perilune (days)", "Apogee (days)",
                "Body contact (days)"};
}

public class ParamPanel
{
    public Label[] ls;
    public ArrowEntry[] es;
    public Entry eTime;
    public Button panelButton;

    internal readonly string strEntry = "##0.0#######";
    internal double DistanceScale { get; set; } = 1.0;
    internal double AltitudeOffset { get; set; } = 0.0;
    internal double TimeScale { get; set; } = 1.0;

    public virtual void GetFromTBP(EMCRTBP tbp) =>
        throw new System.NotImplementedException();
    public virtual void SetIntoTBP(EMCRTBP tbp) =>
        throw new System.NotImplementedException();

    public void ResetFormats()
    {
        foreach (int i in Enumerable.Range(0, es.Length))
            es[i].format = strEntry;
    }
}

public class CartesianPanel : ParamPanel
{
    public CartesianPanel()
    {
        ls = new Label[4] { new Label("x:"), new Label("y:"),
                new Label("ẋ:"), new Label("ẏ:") };
        es = new ArrowEntry[ls.Length];
        foreach (int i in Enumerable.Range(0, ls.Length))
            es[i] = new ArrowEntry(this);
    }

    public override void SetIntoTBP(EMCRTBP tbp)
    {
        try
        {
            foreach (int i in Enumerable.Range(0, es.Length))
            {
                tbp.InitialState[i] = Convert.ToDouble(es[i].Text);
                es[i].SetFormatLike(es[i].Text);

            }
        }
        catch
        {
            GetFromTBP(tbp);
        }
    }
}

public class InitialCartesianPanel : CartesianPanel
{
    public override void GetFromTBP(EMCRTBP tbp)
    {
        foreach (int i in Enumerable.Range(0, es.Length))
            es[i].Text = tbp.InitialState[i].ToString(es[i].format);
    }
}

public class FinalCartesianPanel : CartesianPanel
{
    public override void GetFromTBP(EMCRTBP tbp)
    {
        foreach (int i in Enumerable.Range(0, es.Length))
            es[i].Text = tbp.FinalState[i].ToString(strEntry);
        eTime.Text = tbp.FinalTime.ToString(strEntry);
    }
}

public class BodyCentricPanel : ParamPanel
{
    public BodyCentricPanel()
    {
        ls = new Label[4] { new Label("r:"), new Label("θ:"),
                new Label("v:"), new Label("γ:") };
        es = new ArrowEntry[ls.Length];
        foreach (int i in Enumerable.Range(0, ls.Length))
            es[i] = new ArrowEntry(this);
    }

    internal void SetIntoTBPWithOffset(EMCRTBP tbp, double offset = 0)
    {
        double f = 180 / Math.PI;
        try
        {
            double r = (Convert.ToDouble(es[0].Text) +
                AltitudeOffset) / DistanceScale;
            double θ = Convert.ToDouble(es[1].Text) / f;
            double v = Convert.ToDouble(es[2].Text) / TimeScale;
            double γ = Convert.ToDouble(es[3].Text) / f;

            tbp.InitialState[0] = r * Math.Cos(θ) - offset;
            tbp.InitialState[1] = r * Math.Sin(θ);
            tbp.InitialState[2] = v * Math.Cos(γ);
            tbp.InitialState[3] = v * Math.Sin(γ);
        }
        catch
        {
            GetFromTBP(tbp);
        }
        foreach (int i in Enumerable.Range(0, es.Length))
            es[i].SetFormatLike(es[i].Text);

    }

    internal void SetBodyCentricEntriesFromCart(double[] cs, double offset)
    {
        double[] gs = new double[4];
        double f = 180 / Math.PI;
        gs[0] = (Math.Pow(Math.Pow(cs[0] +
            offset, 2) + Math.Pow(cs[1], 2), 0.5) * DistanceScale) -
            AltitudeOffset;
        gs[1] = f * Math.Atan2(cs[1], cs[0] + offset);
        gs[2] = Math.Pow(Math.Pow(cs[2], 2) + Math.Pow(cs[3], 2), 0.5) *
            TimeScale;
        gs[3] = f * Math.Atan2(cs[3], cs[2]);
        foreach (int i in Enumerable.Range(0, es.Length))
            es[i].Text = gs[i].ToString(es[i].format);
    }
}

public class GeocentricPanel : BodyCentricPanel
{
    public override void SetIntoTBP(EMCRTBP tbp) =>
        SetIntoTBPWithOffset(tbp, tbp.emc.MuMoon);
}

public class InitialGeocentricPanel : GeocentricPanel
{
    public override void GetFromTBP(EMCRTBP tbp) =>
        SetBodyCentricEntriesFromCart(tbp.InitialState, tbp.emc.MuMoon);
}

public class FinalGeocentricPanel : GeocentricPanel
{
    public override void GetFromTBP(EMCRTBP tbp)
    {
        SetBodyCentricEntriesFromCart(tbp.FinalState, tbp.emc.MuMoon);
        eTime.Text = tbp.FinalTime.ToString(strEntry);
    }
}

public class SelenocentricPanel : BodyCentricPanel
{
    public override void SetIntoTBP(EMCRTBP tbp) =>
        SetIntoTBPWithOffset(tbp, -tbp.emc.MuEarth);
}

public class InitialSelenocentricPanel : SelenocentricPanel
{
    public override void GetFromTBP(EMCRTBP tbp) =>
        SetBodyCentricEntriesFromCart(tbp.InitialState, -tbp.emc.MuEarth);
}

public class FinalSelenocentricPanel : SelenocentricPanel
{
    public override void GetFromTBP(EMCRTBP tbp)
    {
        SetBodyCentricEntriesFromCart(tbp.FinalState, -tbp.emc.MuEarth);
        eTime.Text = tbp.FinalTime.ToString(strEntry);
    }
}

public class InitialLeoPanel : InitialGeocentricPanel
{
    public InitialLeoPanel()
    {
        var emc = new EMConst();
        DistanceScale = EMConst.EmDistance;
        TimeScale = 1.025;
        AltitudeOffset = emc.EarthRadius * DistanceScale;
        ls = new Label[4] { new Label("a:"), new Label("θ:"),
                new Label("v:"), new Label("γ:") };
        es = new ArrowEntry[ls.Length];
        foreach (int i in Enumerable.Range(0, ls.Length))
            es[i] = new ArrowEntry(this);
    }
}

public class InitialLloPanel : InitialSelenocentricPanel
{
    public InitialLloPanel()
    {
        var emc = new EMConst();
        DistanceScale = EMConst.EmDistance;
        TimeScale = 1.025;
        AltitudeOffset = emc.MoonRadius * DistanceScale;
        ls = new Label[4] { new Label("a:"), new Label("θ:"),
                new Label("v:"), new Label("γ:") };
        es = new ArrowEntry[ls.Length];
        foreach (int i in Enumerable.Range(0, ls.Length))
            es[i] = new ArrowEntry(this);
    }
}

public class FinalLeoPanel : FinalGeocentricPanel
{
    public FinalLeoPanel()
    {
        var emc = new EMConst();
        DistanceScale = EMConst.EmDistance;
        TimeScale = 1.025;
        AltitudeOffset = emc.EarthRadius * DistanceScale;
        ls = new Label[4] { new Label("a:"), new Label("θ:"),
                new Label("v:"), new Label("γ:") };
    }
}

public class FinalLloPanel : FinalSelenocentricPanel
{
    public FinalLloPanel()
    {
        var emc = new EMConst();
        DistanceScale = EMConst.EmDistance;
        TimeScale = 1.025;
        AltitudeOffset = emc.MoonRadius * DistanceScale;
        ls = new Label[4] { new Label("a:"), new Label("θ:"),
                new Label("v:"), new Label("γ:") };
    }
}

public class TrajectoryPanel : ParamPanel
{
    public virtual double Units { get; } = 1.0;

    public TrajectoryPanel()
    {
        ls = new Label[1] { new Label("max t:") };
        es = new ArrowEntry[ls.Length];
        foreach (int i in Enumerable.Range(0, ls.Length))
            es[i] = new ArrowEntry(this);
    }
    public override void GetFromTBP(EMCRTBP tbp)
    {
        es[0].Text = (tbp.MaxTime * Units).ToString(es[0].format);
        tbp.TimeUnits = Units;
    }

    public override void SetIntoTBP(EMCRTBP tbp)
    {
        try
        {
            tbp.MaxTime = Convert.ToDouble(es[0].Text) / Units;
            es[0].SetFormatLike(es[0].Text);
        }
        catch
        {
            GetFromTBP(tbp);
        }
    }

    public virtual bool End(EMCRTBP tbp, double rEarth, double rMoon)
    {
        return false;
    }

}

public class SiderealTrajectory : TrajectoryPanel
{
}

public class DaysTrajectory : TrajectoryPanel
{
    public override double Units => 27.321662;
}

public class HoursTrajectory : TrajectoryPanel
{
    public override double Units => 655.7198889;
}

public class PeriluneDays : TrajectoryPanel
{
    private bool approaching = false;
    public override double Units => 27.321662;
    public override bool End(EMCRTBP tbp, double rEarth, double rMoon)
    {
        double Perilune = tbp.ApsisSummary.Perilune;
        double Apolune = tbp.ApsisSummary.Apolune;

        if (rMoon < Apolune) approaching = true;
        if (approaching && rMoon > Perilune)
        {
            approaching = false;
            return true;
        }
        return false;
    }
}

public class ApogeeDays : TrajectoryPanel
{
    private bool approaching = true;
    public override double Units => 27.321662;
    public override bool End(EMCRTBP tbp, double rEarth, double rMoon)
    {
        double Perigee = tbp.ApsisSummary.Perigee;
        double Apogee = tbp.ApsisSummary.Apogee;

        if (rEarth > Perigee) approaching = false;
        if (!approaching && rEarth < Apogee)
        {
            approaching = true;
            return true;
        }
        return false;
    }
}
public class BodyContactDays : TrajectoryPanel
{
    public override double Units => 27.321662;

    public override bool End(EMCRTBP tbp, double rEarth, double rMoon)
    {
        var emc = new EMConst();
        if (rEarth < emc.EarthRadius || rMoon < emc.MoonRadius)
            return true;
        return false;
    }
}
