﻿
TODO Features:
✓ trajectory option "Until Perilune"
✓ "Set" button on initial panel
✓ "Transfer" button on final panel
- view...steps showing trajectory step numerical values 
- view...show lagrange points
✓ units... km, km/s -- per sub-panel
✓ units... altitude vs. radius for body-centric panels
✓ geocentric r,θ,v,γ or barycentric position panels
✓ selenocentric position panels
- allow locking γ to be always orthogonal to θ, or
- body-centric panel with r, θ and injection characteristic energy C3
✓ time units days, hours (vs. siderial months)
✓ display perilune and perigee below plot
- display times of apsis events in summary line
- display times of apsis events or other watch events as a panel
✓ view...zoom
✓ view...center
- color cycle options: monthly, once, never
- allow some sub-panels (e.g. final conditions)to be hidden or rolled up
- convert apsis display to a "Watch State" sub-panel
- "First periapsis after apoapsis" as a checkbox? Or always? Or "first apsis?"
- Store.Initial State..., Store.Final State..., Store.Restore... menus.
- File.Save, File.Load, i.e. serialize app state
✓ End condition "On body contact"
✓ preserve the user's sense of precision for each entry field.
✓ make Enter key equivalent to Set button
- cursor hover over trajectory point brings up tooltip showing trajectory state
✓ move max step size to a settings...integration dialog
- add the other options (precision settings) to integration dialog
x model...MaxStep, etc. options of Ode.RK547M

TODO Implementation:
- rework "stop at apsis" functionality to avoid repeated code.
- convert panels to use Table widgets
- convert panels to be dialog widgets


FIXME:
✓ transfer needs to reset displayed precisions
- apoapsis and periapsis is buggy when using "Transfer" button.
- set scale and offset into Cairo Transform and then forget about them.
- fix what happens when negative maxTime is entered.

REFERENCES:
For the equations of motion as used, see e.g. page 15:
Celestial Mechanics Notes Set 4: The Circular
Restricted Three Body Problem
J.D. Mireles James
December 19, 2006
http://cosweb1.fau.edu/~jmirelesjames/hw4Notes.pdf

See also the implementation in pcr3bp.m et al. from: 
http://www.dept.aoe.vt.edu/~sdross/books/dynamics_CR3BP.zip
as described in:
http://www.dept.aoe.vt.edu/~sdross/books/KoLoMaRo_DMissionBook_2011-04-25.pdf
