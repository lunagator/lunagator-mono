﻿// EMCRTBP -
//  Show a spacecraft trajectory in the context of the Earth and 
//  Moon, drawn to scale. Calculate the trajectory using a Circular
//  Restricted Three Body Problem model.

using System.Collections.Generic;
using Microsoft.Research.Oslo;
using System;

public class EMCRTBP
{
    public readonly EMConst emc = new EMConst();
    private static readonly ClrConst clrc = new ClrConst();
    public double MaxTime { get; set; } = 0.2264;
    public double MaxStep { get; set; } = 0.5;
    public Apsides ApsisSummary { get; set; }
    public double[] FinalState { get; private set; } = { 0, 0, 0, 0 };
    public double FinalTime { get; private set; } = 0;
    public double TimeUnits { get; set; } = 1;
    public double[] InitialState { get; set; } = { -0.023335, -0.012865,
        8.058825, -7.00543 };
    public double ViewZoom { get; set; } = 1;
    public double ViewOffset { get; set; } = 0;
    public int ViewCenter { get; set; } = 0;

    public Vector CRTBP(double t, Vector x)
    {
        double dEarth = Math.Pow(Math.Pow(x[0] + emc.MuMoon, 2) + 
            Math.Pow(x[1], 2), 1.5);
        double dMoon = Math.Pow(Math.Pow(x[0] - emc.MuEarth, 2) + 
            Math.Pow(x[1], 2), 1.5);
        double Ux = -x[0] + (emc.MuEarth * (x[0] + emc.MuMoon) / dEarth) +
            emc.MuMoon * (x[0] - emc.MuEarth) / dMoon;
        double Uy = -x[1] + (emc.MuEarth * x[1] / dEarth) +
            emc.MuMoon * x[1] / dMoon;

        return new Vector(x[2], x[3], 2 * x[3] - Ux, -2 * x[2] - Uy);
    }

    public void Draw(Cairo.Context g, int width, int height, 
        Func<EMCRTBP, double, double, bool> End)
    {
        double scale = 2.5 / (ViewZoom * width);
        g.LineWidth = 1;
        g.Transform(new Cairo.Matrix(1, 0, 0, -1, (width / 2) - (ViewOffset / scale), height / 2));

        DrawCelestialBody(g, clrc.EarthColor, emc.EarthRadius, -emc.MuMoon, scale);
        DrawCelestialBody(g, clrc.MoonColor, emc.MoonRadius, emc.MuEarth, scale);

        DrawDot(g, InitialState[0], InitialState[1], scale);
        g.MoveTo(InitialState[0] / scale, InitialState[1] / scale);
        ApsisSummary = new Apsides();
        var solOpts = new Options { MaxStep = MaxStep, 
            RelativeTolerance = 1e-6, AbsoluteTolerance = 1e-6 };
        IEnumerable<SolPoint> sol = Ode.RK547M(0, new Vector(InitialState),
            CRTBP, solOpts);
        try
        {
            double maxTime = MaxTime * 2 * Math.PI;
            foreach (SolPoint sp in sol.SolveTo(maxTime))
            {
                int nColors = clrc.clrsRainbow.Length;

                g.LineTo(sp.X[0] / scale, sp.X[1] / scale);
                g.Stroke();
                DrawDot(g, sp.X[0], sp.X[1], scale,
                    (int)(nColors * sp.T / (2 * Math.PI)) % nColors);
                g.LineTo(sp.X[0] / scale, sp.X[1] / scale);
                g.Stroke();
                g.MoveTo(sp.X[0] / scale, sp.X[1] / scale);

                double rEarth = Math.Pow(Math.Pow(sp.X[0] + emc.MuMoon, 2) +
                    Math.Pow(sp.X[1], 2), 0.5);
                double rMoon = Math.Pow(Math.Pow(sp.X[0] - emc.MuEarth, 2) +
                    Math.Pow(sp.X[1], 2), 0.5);
                if (End(this, rEarth, rMoon)) return;
                ApsisSummary.Update(rEarth, rMoon);
                SetFinalValues(sp.X, sp.T);
            }
        } catch { }
    }

    private static void DrawCelestialBody(Cairo.Context g,
        Cairo.Color bodyColor, double radius, double x, double scale)
    {
        g.SetSourceRGB(0.75, 0.25, 0.0);
        g.Arc(x / scale, 0, radius / scale, 0, 2 * Math.PI);
        g.StrokePreserve();
        g.SetSourceRGB(bodyColor.R, bodyColor.G, bodyColor.B);
        g.Fill();
    }

    private static void DrawDot(Cairo.Context g, double x, double y,
        double scale, int clrIndex = 0)
    {
        Cairo.Color clr = clrc.clrsRainbow[clrIndex];

        g.SetSourceRGB(clr.R, clr.G, clr.B);
        g.Arc(x / scale, y / scale, 2.0, 0, 2 * Math.PI);
        g.StrokePreserve();
        g.Fill();
    }

    private void SetFinalValues(double[] value, double t)
    {
        FinalState = value;
        FinalTime = (TimeUnits * t / (2 * Math.PI));
    }
}
