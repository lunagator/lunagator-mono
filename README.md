# Lunagator #

Explore spacecraft trajectories in the vicinity of the Earth and Moon.

### Lunagator for Mono ###

* Mono is an open source implementation of Microsoft's .NET Framework.
* This repository provides source code for a Mono desktop app, "Lunagator."
* Lunagator for Mono uses gtk-sharp UI widgets and draws with Mono.Cairo.
* This is "alpha" quality pre-release code. Version 0.2. Use at your own risk.

### Project Goals ###

* Within the context of a 2D model (i.e. motion within the plane of the Earth-Moon system):
* Establish UI functionality that would make sense for a 3D model

### Building the App ###

* The Mono framework is available for download from https://www.mono-project.com/
* To calculate trajectories the app uses the OSLO Open Solving Library for ODEs 
* OSLO is provided by Microsoft Research at https://www.microsoft.com/en-us/research/project/open-solving-library-for-odes/

### Contribution Guidelines ###

* Contributions are welcome!

### Who do I talk to? ###

* Contact sdsds-spaceflight@protonmail.com
* Discuss on https://forum.nasaspaceflight.com/