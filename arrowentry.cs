﻿using System;
using System.Linq;
using Gtk;

[Binding(Gdk.Key.Up, "UpKey")]
[Binding(Gdk.Key.Down, "DownKey")]

public class ArrowEntry : Entry
{
    private ParamPanel panel;
    public String format;

    public ArrowEntry(ParamPanel pp)
    {
        Activated += ArrowEntry_Activated;
        panel = pp;
        format = panel.strEntry;
    }

    void ArrowEntry_Activated(object sender, EventArgs e) => 
        panel.panelButton.Activate();

    (int before, int after) ParseDigits(String value)
    {
        int b, a;
        if (value.StartsWith("-", StringComparison.Ordinal))
            value = value.Substring(1);
        b = value.IndexOf('.');
        if (-1 == b)
            return (value.Length, 0);
        a = value.Substring(b).Length - 1;
        return (b, a);
    }

    public void SetFormatLike(String value)
    {
        int digitsBefore, digitsAfter;

        (digitsBefore, digitsAfter) = ParseDigits(value);
        format = string.Empty;
        foreach (int i in Enumerable.Range(1, digitsAfter))
            format = format.Insert(0, "0");
        format = format.Insert(0, ".");
        foreach (int i in Enumerable.Range(1, digitsBefore))
            format = format.Insert(0, "0");
    }

    private double ArrowAdjustment(String value)
    {
        int digitsBefore, digitsAfter;

        (digitsBefore, digitsAfter) = ParseDigits(value);
        return Math.Pow(10.0, (double)-digitsAfter);
    }

    private void UpKey()
    {
        if (!IsEditable) return;
        try
        {
            double n = Convert.ToDouble(Text) + ArrowAdjustment(Text);
            Text = n.ToString(format);
            panel.panelButton.Activate();
        }
        catch { }
    }

    private void DownKey()
    {
        if (!IsEditable) return;
        try
        {
            double n = Convert.ToDouble(Text) - ArrowAdjustment(Text);
            Text = n.ToString(format);
            panel.panelButton.Activate();
        }
        catch { }
    }
}
