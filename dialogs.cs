﻿using System;
using Gtk;

partial class EarthMoon : Window
{
    protected static void OnAbout(object sender, EventArgs e)
    {
        AboutDialog dialog = new AboutDialog
        {
            ProgramName = "Lunagator",
            Version = "0.3",
            Comments = "A Mono app to explore spacecraft trajectories " +
                "in the vicinity of the Earth and Moon.",
            Authors = new string[] { "sdSds" },
            Website = "https://bitbucket.org/lunagator/lunagator-mono/"
        };

        dialog.Run();
        dialog.Destroy();
    }

    private void OnView(object sender, EventArgs e)
    {
        Dialog dialog = new Dialog("View Settings", this,
            DialogFlags.Modal | DialogFlags.DestroyWithParent,
            Gtk.Stock.Ok, ResponseType.Ok, Gtk.Stock.Cancel,
            ResponseType.Cancel);

        Table table = new Table(2, 2, false);
        dialog.VBox.PackStart(table, false, false, 0);
        table.RowSpacing = table.ColumnSpacing = 4;

        Label zoomLabel = new Label("_Zoom:");
        table.Attach(zoomLabel, 0, 1, 0, 1);
        Entry zoomEntry = new Entry(tbp.ViewZoom.ToString("0.0###"));
        table.Attach(zoomEntry, 1, 2, 0, 1);
        zoomLabel.MnemonicWidget = zoomEntry;

        Label centerLabel = new Label("_Center:");
        table.Attach(centerLabel, 0, 1, 1, 2);
        var centerCombo = new ComboBox(new string[] { "Barycenter", "Earth",
            "Moon", "Midway" });
        double[] centerOffsets;
        EMConst emc = new EMConst();
        centerOffsets = new double[] { 0, emc.MuMoon, emc.MuEarth,
            (emc.MuMoon + emc.MuEarth) / 2 };
        table.Attach(centerCombo, 1, 2, 1, 2);
        centerLabel.MnemonicWidget = centerCombo;
        centerCombo.Active = tbp.ViewCenter;

        table.ShowAll();

        if (ResponseType.Ok == (ResponseType)dialog.Run()) try
            {
                tbp.ViewZoom = Convert.ToDouble(zoomEntry.Text);
                tbp.ViewCenter = centerCombo.Active;
                tbp.ViewOffset = centerOffsets[centerCombo.Active];
            }
            catch { }

        dialog.Destroy();
        aPlot.QueueDraw();
    }

    private void OnIntegration(object sender, EventArgs e)
    {
        Dialog dialog = new Dialog("Integration Settings", this,
            DialogFlags.Modal | DialogFlags.DestroyWithParent,
            Gtk.Stock.Ok, ResponseType.Ok, Gtk.Stock.Cancel,
            ResponseType.Cancel);

        Table table = new Table(1, 2, false);
        dialog.VBox.PackStart(table, false, false, 0);
        table.RowSpacing = table.ColumnSpacing = 4;

        Label stepLabel = new Label("_Max Step:");
        table.Attach(stepLabel, 0, 1, 0, 1);
        Entry stepEntry = new Entry(tbp.MaxStep.ToString("0.0###"));
        table.Attach(stepEntry, 1, 2, 0, 1);
        stepLabel.MnemonicWidget = stepEntry;

        table.ShowAll();

        if (ResponseType.Ok == (ResponseType)dialog.Run()) try
            {
                tbp.MaxStep = Convert.ToDouble(stepEntry.Text);
            }
            catch { }

        dialog.Destroy();
        aPlot.QueueDraw();
    }
}