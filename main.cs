﻿using Gtk;

internal class MainClass
{
    public static void Main(string[] args)
    {
        EarthMoon em;

        Application.Init();
        em = new EarthMoon
        {
            Title = "Lunagator"
        };
        Application.Run();
    }
}